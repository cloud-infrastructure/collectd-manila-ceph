Name:           collectd-manila-ceph
Version:        1.0.1
Release:        3%{?dist}
Summary:        Simple collectd exec plugin to retrieve metrics from ceph in a manila node
License:        GPLv3
Group:          Development/Libraries
BuildArch:      noarch
Url:            https://gitlab.cern.ch/cloud-infrastructure/collectd-manila-ceph

Source0:        %{name}-%{version}.tar.gz

Requires:       collectd
Requires:       ceph-common
Requires:       openstack-manila
Requires:       jq

Requires(post):         collectd
Requires(preun):        collectd
Requires(postun):       collectd

%{?systemd_requires}
BuildRequires: systemd

%description
Simple collectd exec plugin that retrieves metrics from ceph in a manila node

%prep
%setup -q -n %{name}-%{version}

%build
%install
install -p -D -m 755 src/collectd-manila-ceph.sh %{buildroot}%{_sbindir}/collectd-manila-ceph.sh

%post
%systemd_post collectd.service

%preun
%systemd_preun collectd.service

%postun
%systemd_postun_with_restart collectd.service

%files
%attr(0755, root, root) %{_sbindir}/collectd-manila-ceph.sh
%doc src/README.md

%changelog
* Fri Feb 09 2024 Ulrich Schwickerath <ulrich.schwickerath@cern.ch> 1.0.1-3
- Build for RHEL9 & ALMA9

* Fri Sep 01 2023 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.1-2
- Build for RHEL & ALMA

* Tue Apr 13 2021 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.1-1
- Initial rebuild for el8s

* Wed Dec 09 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-2
- Change location of binary

* Fri Dec 04 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 1.0.0-1
- Initial release of the manila-ceph plugin
