[![pipeline status](https://gitlab.cern.ch/cloud-infrastructure/collectd-manila-ceph/badges/master/pipeline.svg)](https://gitlab.cern.ch/cloud-infrastructure/collectd-manila-ceph/-/commits/master)

# collectd-manila-ceph

A collectd plugin written in bash to gather ceph metrics from a manila node

The main purpose of this plugin is to send values locally so we can trigger alarms on those if
_deleting subvolumegroup is getting full

It gathers the following information:
  - subvolumes in _nogroup
  - subvolumes in _deleting

## Configuration

And example of configuration file can be found in `config/`. For instance:

```
LoadPlugin exec
<Plugin exec>
  Exec "manila:nogroup" "/usr/sbin/collectd-manila-ceph.sh"
</Plugin>
```
