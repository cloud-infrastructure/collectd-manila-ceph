#!/bin/bash

HOSTNAME="${COLLECTD_HOSTNAME:-$(hostname -f)}"
INTERVAL="${COLLECTD_INTERVAL:-60}"
EXEC_ONCE="${EXEC_ONCE:-0}"
MANILA_CONF="${MANILA_CONF:-"/etc/manila/manila.conf"}"

PREFIX="cfg_section_"

function cfg_parser {
   shopt -p extglob &> /dev/null
   CHANGE_EXTGLOB=$?
   if [ $CHANGE_EXTGLOB = 1 ]
   then
      shopt -s extglob
   fi
   ini="$(<$1)"                 # read the file
   ini=${ini//$'\r'/}           # remove linefeed i.e dos2unix
   ini="${ini//[/\\[}"
   ini="${ini//]/\\]}"
   OLDIFS="$IFS"
   IFS=$'\n' && ini=( ${ini} )  # convert to line-array
   ini=( ${ini[*]/#*([[:space:]]);*/} )
   ini=( ${ini[*]/#*([[:space:]])\#*/} )
   ini=( ${ini[*]/#+([[:space:]])/} ) # remove init whitespace
   ini=( ${ini[*]/%+([[:space:]])/} ) # remove ending whitespace
   ini=( ${ini[*]/%+([[:space:]])\\]/\\]} ) # remove non meaningful whitespace after sections
   ini=( ${ini[*]/*([[:space:]])=*([[:space:]])/=} ) # remove whitespace around =
   ini=( ${ini[*]/#\\[/\}$'\n'"$PREFIX"} ) # set section prefix
   for ((i=0; i < "${#ini[@]}"; i++))
   do
      line="${ini[i]}"
      if [[ "$line" =~ $PREFIX.+ ]]
      then
         ini[$i]=${line// /_}
      fi
   done
   ini=( ${ini[*]/%\\]/ \(} )   # convert text2function (1)
   ini=( ${ini[*]/=/=\( } )     # convert item to array
   ini=( ${ini[*]/%/ \)} )      # close array parenthesis
   ini=( ${ini[*]/%\\ \)/ \\} ) # the multiline trick
   ini=( ${ini[*]/%\( \)/\(\) \{} ) # convert text2function (2)
   ini=( ${ini[*]/%\} \)/\}} )  # remove extra parenthesis
   ini=( ${ini[*]/%\{/\{$'\n''cfg_unset ${FUNCNAME/#'$PREFIX'}'$'\n'} )  # clean previous definition of section
   ini[0]=""                    # remove first element
   ini[${#ini[*]} + 1]='}'      # add the last brace
   eval "$(echo "${ini[*]}")"   # eval the result
   EVAL_STATUS=$?
   if [ $CHANGE_EXTGLOB = 1 ]
   then
      shopt -u extglob
   fi
   IFS="$OLDIFS"
   return $EVAL_STATUS
}

function cfg_unset {
   SECTION=$1
   OLDIFS="$IFS"
   IFS=' '$'\n'
   if [ -z "$SECTION" ]
   then
      fun="$(declare -F)"
   else
      fun="$(declare -F $PREFIX$SECTION)"
      if [ -z "$fun" ]
      then
         echo "section $SECTION not found" 1>&2
         return
      fi
   fi
   fun="${fun//declare -f/}"
   for f in $fun; do
      [ "${f#$PREFIX}" == "${f}" ] && continue
      item="$(declare -f ${f})"
      item="${item##*\{}" # remove function definition
      item="${item##*FUNCNAME*$PREFIX\};}" # remove clear section
      item="${item/\}}"  # remove function close
      item="${item%)*}" # remove everything after parenthesis
      item="${item});" # add close parenthesis
      vars=""
      while [ "$item" != "" ]
      do
         newvar="${item%%=*}" # get item name
         vars="$vars $newvar" # add name to collection
         item="${item#*;}" # remove readed line
      done
      for var in $vars; do
         unset $var
      done
   done
   IFS="$OLDIFS"
}

function put_value() {
  local cluster="$1"
  local type="$2"
  local data="$3"
  echo "PUTVAL \"${HOSTNAME}/manila-${cluster}/gauge-${type}\" interval=${INTERVAL} N:${data}"
}

while : ; do
  # Iterate over the clusters for fetching metrics
  cfg_parser "$MANILA_CONF"
  cfg_section_DEFAULT

  # Loop over enabled_backends
  IFS=', ' read -r -a backends <<< "$enabled_share_backends"
  for backend in "${backends[@]}"; do
    # Load backend configuration
    "cfg_section_${backend}"
    ITEMS=$(/usr/bin/ceph --conf "$cephfs_conf_path" --id "$cephfs_auth_id" fs subvolume ls cephfs 2>/dev/null | jq length)
    TRASH=$(/usr/bin/ceph --conf "$cephfs_conf_path" --id "$cephfs_auth_id" fs subvolume ls cephfs _deleting 2>/dev/null | jq length)

    put_value "$cephfs_cluster_name" "items" "${ITEMS:-0}"
    put_value "$cephfs_cluster_name" "trash" "${TRASH:-0}"
  done

  if [ "$EXEC_ONCE" -eq 1 ]; then
    break
  fi
  sleep "$INTERVAL"
done

exit 0
